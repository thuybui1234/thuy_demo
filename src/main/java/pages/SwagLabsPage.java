package pages;

import common.CommonUltils;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import webKeyword.WebKeyWords;

public class SwagLabsPage extends WebKeyWords {
    @FindBy(xpath = "//*[@class='shopping_cart_link']//span")
    private WebElement QUANTITY_ITEMS;
    @FindBy(xpath = "//*[@class='shopping_cart_container']")
    private WebElement CART_ITEMS;

    public static final String CLICK_ADD_TO_CART = "//div[@class='inventory_item'][%s]//button[text()='Add to cart']";
    public static final String REMOVE_BUTTON = "//div[@class='inventory_item'][%s]//button[text()='Remove']";

    @Step("click add to card with random items")
    public SwagLabsPage clickAddToCart() {
        int number = 0;
        for (int i = 0; i < 2; i++) {
            int number1 = CommonUltils.getRandomValue(6);
            while (number == number1) {
                number1 = CommonUltils.getRandomValue(6);
            }
            number = number1;
            waitForElementVisible(CLICK_ADD_TO_CART, String.valueOf(number));
            if (getTextElement(CLICK_ADD_TO_CART, String.valueOf(number)).equals("Add to cart")) {
                clickToElementByJavascript(CLICK_ADD_TO_CART, String.valueOf(number));
                verifyItemStatusChanged(number);
            }
        }
        return this;
    }

    @Step("verify item status changed")
    public SwagLabsPage verifyItemStatusChanged(int number) {
        waitForElementVisible(REMOVE_BUTTON, String.valueOf(number));
        Assert.assertTrue(isControlDisplayed(REMOVE_BUTTON, String.valueOf(number)));
        return this;
    }

    @Step("Verify Quantity {0} Items Is Correct")
    public SwagLabsPage verifyQuantityItemsIsCorrect(String quantity) {
        String number = getTextElement(QUANTITY_ITEMS);
        Assert.assertEquals(number, quantity);
        return this;
    }

    @Step("click on cart button")
    public SwagLabsPage clickOnCart() {
        clickToElementByAction(CART_ITEMS);
        return this;
    }

    public SwagLabsPage verifyRedirectToInventory() {
        String url = getCurrentPageUrl();
        waitForPageLoaded();
        Assert.assertEquals(url, "https://www.saucedemo.com/inventory.html");
        return this;
    }

}
