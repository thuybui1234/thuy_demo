package report;


import com.google.common.collect.ImmutableMap;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;

public class AllureManager {

    public static void setAllureEnvironmentInformation(String browser, String url) {
        allureEnvironmentWriter(
                ImmutableMap.<String, String>builder()
                        .put("Browser", browser)
                        .put("URL", url)
                        .build(), System.getProperty("user.dir")
                        + "/target/allure-results/");
    }
}
