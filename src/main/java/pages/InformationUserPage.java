package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import webKeyword.WebKeyWords;

public class InformationUserPage extends WebKeyWords {
    @FindBy(xpath = "//input[@placeholder = 'First Name']")
    public WebElement FIRST_NAME;
    @FindBy(xpath = "//input[@placeholder = 'Last Name']")
    public WebElement LAST_NAME;
    @FindBy(xpath = "//input[@placeholder = 'Zip/Postal Code']")
    public WebElement POSTAL_CODE;
    @FindBy(xpath = "//input[@name='continue']")
    public WebElement CONTINUE_BUTTON;

    @Step("verify Redirect To Information User")
    public InformationUserPage verifyRedirectToInformationUser(){
        String url = getCurrentPageUrl();
        Assert.assertEquals(url, "https://www.saucedemo.com/checkout-step-one.html");
        return this;
    }

    @Step("input first name with value {0}")
    public InformationUserPage inputFirstName(String firstName){
        sendKeyToElement(FIRST_NAME,firstName);
        return this;
    }

    @Step("input last name with value {0}")
    public InformationUserPage inputLastName(String lastName){
        sendKeyToElement(LAST_NAME, lastName);
        return this;
    }

    @Step("input Postal Code with value {0}")
    public InformationUserPage inputPostalCode(String postalCode){
        sendKeyToElement(POSTAL_CODE, postalCode);
        return this;
    }

    @Step("click on Continue button")
    public InformationUserPage clickOnContinue(){
        clickToElement(CONTINUE_BUTTON);
        return this;
    }

}
