package tests;

import excel.ExcelConfig;
import listener.AbstractWebTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestDemo extends AbstractWebTest {
    ExcelConfig excelConfig = new ExcelConfig("src/test/resources/Demo_Auto.xlsx");

    @Test(description="Log in the web", priority = 1, dataProvider = "data")
    public void LogInPage(String username, String password) {
        logInPage.inputInFo(username, password)
                .clickLogIn();
        swagLabsPage.verifyRedirectToInventory();

    }
    @Test(description = "buy product with random items", priority = 1, dataProvider = "data")
    public void buyProduct(String username, String password){
        logInPage.inputInFo(username, password)
                .clickLogIn();
        swagLabsPage.verifyRedirectToInventory()
                .clickAddToCart()
                .verifyQuantityItemsIsCorrect("2");
    }

    @Test(description = "check out items", priority = 2, dataProvider = "data")
    public void checkOutItems(String username, String password){
        logInPage.inputInFo(username, password)
                .clickLogIn();
        swagLabsPage.verifyRedirectToInventory()
                .clickAddToCart()
                .verifyQuantityItemsIsCorrect("2")
                .clickOnCart();
        cartPage.verifyCartItemSelected()
                .verifyURLIsCorrect()
                .clickOnCheckOut();
    }
    @Test(description = "check out complete items and redirect to inventory page", priority = 2, dataProvider = "data")
    public void checkOutComplete(String username, String password){
        logInPage.inputInFo(username, password)
                .clickLogIn();
        swagLabsPage.verifyRedirectToInventory()
                .clickAddToCart()
                .verifyQuantityItemsIsCorrect("2")
                .clickOnCart();
        cartPage.verifyCartItemSelected()
                .verifyURLIsCorrect()
                .clickOnCheckOut();
        informationUserPage.verifyRedirectToInformationUser()
                .inputFirstName("Thuy")
                .inputLastName("Bui")
                .inputPostalCode("2506")
                .clickOnContinue();
        checkOutPage.verifyRedirectToCheckOutOverView()
                .clickOnFinishButton();
        checkOutCompletePage.verifyCheckOutComplete()
                .clickOnBackHomeButton();
        swagLabsPage.verifyRedirectToInventory();
    }
    @DataProvider(name = "data")
    public Object[][] data() {
        return excelConfig.data();
    }
}
