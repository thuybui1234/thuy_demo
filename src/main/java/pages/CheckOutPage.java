package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import webKeyword.WebKeyWords;

public class CheckOutPage extends WebKeyWords {

    @FindBy(xpath = "//button[text()='Finish']")
    public WebElement FINISH_BUTTON;

    @Step("verify Redirect to checkout OverView")
    public CheckOutPage verifyRedirectToCheckOutOverView(){
        String url = getCurrentPageUrl();
        Assert.assertEquals(url, "https://www.saucedemo.com/checkout-step-two.html");
        return this;
    }

    @Step("click on finish button")
    public CheckOutPage clickOnFinishButton(){
        clickToElement(FINISH_BUTTON);
        return this;
    }

}
