package config;

import java.io.IOException;
import java.util.Properties;

public class ConfigSettings {
	public static ConfigSettings config;
	
	private static final String BROWSER = "browser";
	private static final String DEFAULT_TIMEOUT = "timeout";
	private static final String DEFAULT_URL = "url";
	private static final String SHORT_TIMEOUT = "shortTimeout";
	
	private Properties configProperties;
	
	public ConfigSettings(String projectDirLocator) {
		try {
			setConfigProperties(PropertySettingStoreUtil.getSettings(projectDirLocator, ConfigSettings.class.getSimpleName()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Properties getConfigProperties() {
		if(configProperties == null) {
			configProperties = new Properties();
		}
		return configProperties;
	}


	public String getDefaultTimeout() {
		return this.configProperties.getProperty(DEFAULT_TIMEOUT);
	}
	public String getShortTimeout() {
		return this.configProperties.getProperty(SHORT_TIMEOUT);
	}
	public String getUrl() {
		return this.configProperties.getProperty(DEFAULT_URL);
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

}
