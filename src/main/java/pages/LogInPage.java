package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import webKeyword.WebKeyWords;

public class LogInPage extends WebKeyWords {

    @FindBy(xpath = "//input[@data-test='username']")
    private WebElement USER_NAME;
    @FindBy(xpath = "//input[@data-test='password']")
    private WebElement PASSWORD;
    @FindBy(xpath = "//input[@value='Login']")
    private WebElement LOGIN;

    @Step("input info with username {0} and password {1}")
    public LogInPage inputInFo(String username, String password){
        sendKeyToElement(USER_NAME, username);
        sendKeyToElement(PASSWORD, password);
        return this;
    }
    @Step("click on LogIn button")
    public LogInPage clickLogIn(){
        clickToElement(LOGIN);
        return this;
    }

}
