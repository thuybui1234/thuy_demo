package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import webKeyword.WebKeyWords;

import java.util.List;

public class CartPage extends WebKeyWords {

    @FindBy(xpath = "//div[@class = 'cart_list']//*[@class = 'cart_item']")
    private List<WebElement> CART_ITEMS;
    @FindBy(xpath = "//button[text()='Checkout']")
    private WebElement CHECK_OUT;

    @Step("verify Cart Item selected ")
    public CartPage verifyCartItemSelected(){
        waitForElementVisible(CART_ITEMS.get(0));
        Assert.assertEquals(CART_ITEMS.size(), 2);
        return this;
    }
    @Step("Verify URL /cart.html correct")
    public CartPage verifyURLIsCorrect(){
        String url = getCurrentPageUrl();
        Assert.assertEquals(url, "https://www.saucedemo.com/cart.html");
        return this;
    }
    @Step("click on check out button")
    public CartPage clickOnCheckOut(){
        waitForElementVisible(CHECK_OUT);
        clickToElement(CHECK_OUT);
        return this;
    }
}
